FROM node:12-slim AS builder
WORKDIR /app
COPY . .
RUN yarn install && \
    npm run build

# nginx base image
FROM nginx:1.18.0

# Install tcpdump, htop 
RUN apt-get update && apt-get install -y tcpdump htop

ARG PORT=8080
ENV PORT=${PORT}

EXPOSE ${PORT}

# add permissions for nginx user
RUN chown -R nginx:nginx /var/cache/nginx && \
    chown -R nginx:nginx /var/log/nginx && \
    chown -R nginx:nginx /etc/nginx/conf.d
RUN touch /var/run/nginx.pid && \
        chown -R nginx:nginx /var/run/nginx.pid

# Copy nginx configuration
COPY nginx/nginx.conf /etc/nginx/nginx.conf

COPY nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf

# Copy build directory
COPY --from=builder --chown=nginx:nginx /app/build /usr/share/nginx/html

# Run as non-root user
USER nginx

CMD [ "nginx", "-g", "daemon off;" ]
